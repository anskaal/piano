
module.exports = function(){

  var pianoControl = {
    keyStart : 0, 
    keyStop : 0,
    lastKey : 0,
    lastKeyN : 0,
    treshold : 0,
    triggers : {
      "15" : function(){

        player.stop();
        
        recorder.start();
        recorder.setProcessOffset(1);
      },
      "24" : function(){

        player.playKey("90 24 40\n90 24 40");
        setTimeout(function(){
          player.playKey("90 24 00\90 24 00");
        }, 1000);

        
      },
      "6C" : function(){

        if(!player.isPlaying){
          recorder.stop();
          recorder.removeLastNotes(pianoControl.treshold);
          player.open(recorder.getData());
          
          player.start();
        }
        else{
          player.togglePause();
        }
      },
      "6B" : function(){
        player.looping = !player.looping;
        console.log("Player looping: "+ player.looping);
      }
    },
    clear : function(){
      this.lastKeyN = 0;
    },
    process : function(key, volume){
       
      if(this.triggers[key]){
        
        if(volume != "00"){
        
          this.triggers[key]();
          
          setTimeout(function(){
            terminal.spawn('amidi', ['-p',midiDev, '-S', "90 "+key+" 40"]);
          }, 50);
          setTimeout(function(){
            terminal.spawn('amidi', ['-p',midiDev, '-S', "90 "+key+" 00"]);
          }, 60);
          
          this.clear();
        }
      }
    }
  };

  return pianoControl;
  
};
