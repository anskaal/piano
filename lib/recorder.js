

module.exports = function(){

  return {
    rec : {},
    stopped : true,
    startTimestamp : 0,
    processOffset : 0,
    start : function(){
      this.rec = {};
      this.isRecording = true;
    },
    setProcessOffset : function(o){
      this.processOffset = o;
    },
    process : function(arr){
      if(this.isRecording){
        if(this.processOffset-- <= 0){
          if(this.startTimestamp == 0){
             this.startTimestamp = new Date().getTime();
          }
          this.rec[(new Date().getTime() - this.startTimestamp)] = arr;
        }
      }
    },
    stop : function(){
      this.isRecording = false;
      this.startTimestamp = 0;
      this.stopped = true;
    },
    getData : function(){
      return this.rec;
    },
    removeLastNotes : function(n){
    
      console.log("skipping "+n+" notes...");
      var a = {};
      var i = 0;
      
      var m = 0;
      for(var k in this.rec){
        m++;
      }
      
      for(var k in this.rec){
        if(i++ <= m-n){
          a[k] = this.rec[k];
        }
        else{
          console.log("skipping...");
        }
      }
      
      this.rec = a;
    }
  };
}
