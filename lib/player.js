
module.exports = function(midiDev, io){

  var terminal = require('child_process');
  
  var player = {
    notes : {},
    notetime : [],
    isPlaying : false,
    isPaused : false,
    looping : false,
    pausedAt : 0,
    tsStart : 0,
    open : function(notes){
      this.notes = notes;
      
      // make timing possible
      this.notetime = [];
      for(var ts in notes){
        this.notetime.push(ts);
      }
      
    },
    start : function(){
      if(!this.isPlaying){
        this.isPlaying = true;
        this.isPaused = false;
        this.tsStart = this.now();
        this.sound(0);
      }
      else{
        //console.log("Player already started!");
      }
    },
    stop : function(){
      this.isPlaying = false;
      this.isPaused = false;
    },
    togglePause : function(){
    
      this.isPaused = !this.isPaused;
      
      if(!this.isPaused){
        console.log("Resuming...");
        this.onResume();
      }
      else{
        console.log("Pausing...");
      }
    },
    onResume : function(){
      this.sound(this.pausedAt);
    },
    now : function(){
      return new Date().getTime();
    },
    sound : function(i){
      
      if(!this.isPaused){
        this.sendSound(i);
      }
      else{
        this.pausedAt = i;
        return false;
      }
          
      if(i+1 < this.notetime.length){
        var delta = this.notetime[i+1] - this.notetime[i]; 
        setTimeout(function(){
          player.sound(i+1);
        }, delta);
      }
      else{
        console.log("done!");
        if(this.looping){
          this.sound(0);
        }
        else{
          this.isPlaying = false;
        }
      }
    },
    sendSound : function(i){
      var str = '';
        
      var no = this.notes[this.notetime[i]];
        
      for(var note in no){
        str += no[note].f+' '+no[note].key+' '+no[note].volume+'';
      }
      
      terminal.spawn('amidi', ['-p',midiDev, '-S', str]);
      io.emit('onKey', {'player' : "#09f", 'notes' : no});
    },
    playKey : function(str){
      terminal.spawn('amidi', ['-p',midiDev, '-S', str]);
    }
  };
  
  return player;

};
