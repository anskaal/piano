var app = require('express')();
var ht = require('http');
var http = ht.Server(app);
var path = require('path');
var express = require('express');
var io = require('socket.io')(http);
var fs = require("fs");
var PORT = 3011;
var midiDev = "hw:2";

app.use(express.static(path.join(__dirname, '/')));

app.get('/', function(req, res){
  res.sendFile(path.resolve('./piano.html'));
});

var player = require("./lib/player")(midiDev, io);
var recorder = require("./lib/recorder")();
var pianoControl = require("./lib/pianoControl")();

io.on('connect', function(socket){
  console.log("connect?!");

  socket.on('play', function(data){
    recorder.stop();
    player.open(recorder.getData());
    player.start();
  });

  socket.on('record', function(data){
    recorder.start();
  });

  if(!isReadingPiano){
    readPiano();
  }

});

var spawn = require('child_process').spawn;

var usbObserver = spawn('python', ['listenForUsb.py']);

usbObserver.stdout.on('data', function(data){

});

var isReadingPiano = false;

function readPiano(){

  try{
    if(!isReadingPiano){

      var midi = spawn('amidi', ['-p',midiDev, '-d']);

      midi.stdout.on('data', function (data) {
        isReadingPiano = true;
        console.log("stdout: "+data);
        var lines = (data+"").split('\n');

        var sendArr = [];

        for(var k in lines){

          var arr = lines[k].split(' ');


          if(arr.length > 1){
            var fun = arr[0];
            var key = arr[1];
            var volume = arr[2];

            sendArr.push({'f': fun, 'key': key, 'volume': volume});
            pianoControl.process(key, volume);

          }
        }

        io.emit('onKey', {player : "#f90", notes : sendArr});
        recorder.process(sendArr);

      });

      midi.on('close', function(code, signal) {
        console.log('Child process closed');
        isReadingPiano = false;

        setTimeout(function(){
          readPiano();
        }, 3000);
      });

      midi.on('exit', function(code, signal) {
        console.log('Stopping child process...');
        isReadingPiano = false;

        setTimeout(function(){
          readPiano();
        }, 3000);
      });

    }
  }catch(e){
    console.log(e);
  }
}

http.listen(PORT, function(){
  console.log('listening on *:'+PORT);
});

// Start reading from stdin so we don't exit.
process.stdin.resume();

process.on('SIGINT', function() {
  console.log('Got SIGINT.');
  midi.kill('SIGHUP');

  setTimeout(function(){
    process.exit();
  }, 1000);
});
