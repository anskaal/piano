Piano
=====

Read data from a Piano

Install:
--------
`
$ pacman -S alsa-utils
`

Test that amidi works:
----------------------
`
$ amidi -p hw:1 -d
`

change hw:1 to hw:0 or something if it doesn't
