
var FPS = 60;
var TPF = 1000/FPS;
var isPlaying = 1;
var now;
var scene = {};
var n_stars = 1000;
var sec_stars = 60;
var canvas, ctx;
var nKey = 21;

$( window ).load(function(){

  canvas          = document.getElementById('pianoCanvas');
  canvas.width    = window.innerWidth;
  canvas.height   = window.innerHeight/2;
  ctx             = canvas.getContext('2d');

  var scale = [7,50];
  var y = 25;
  var x = 0;
  
  CH(x,y,scale[0],scale[1],4);
  x+=scale[0]*3;
  CH(x,y,scale[0],scale[1],3);
  x+=scale[0];
  CH(x,y,scale[0],scale[1],2);
  x+=scale[0]*4;
  
  x = create12(scale, x,y);
  x = create12(scale, x,y);
  x = create12(scale, x,y);
  x = create12(scale, x,y);
  x = create12(scale, x,y);
  x = create12(scale, x,y);
  
  CH(x,y,scale[0],scale[1],0);
  x+=scale[0]*2;
  
  setInterval(function() {

    now = new Date();
    
    for(var obj in scene){
      scene[obj].update(TPF);
    }

    ctx.clearRect(0, 0, canvas.width, canvas.width);

    for(var obj in scene){
      scene[obj].draw(ctx);
    }  
    
  }, TPF);
  

});

function create12(scale, x,y){

  CH(x,y,scale[0],scale[1],0);
  x+=scale[0]*2;
  CH(x,y,scale[0],scale[1],3);
  x+=scale[0]*2;
  CH(x,y,scale[0],scale[1],1);
  x+=scale[0]*3;
  CH(x,y,scale[0],scale[1],3);
  x+=scale[0];
  CH(x,y,scale[0],scale[1],2);
  x+=scale[0]*4;
  CH(x,y,scale[0],scale[1],0);
  x+=scale[0]*2;
  CH(x,y,scale[0],scale[1],3);
  x+=scale[0]*2;
  CH(x,y,scale[0],scale[1],4);
  x+=scale[0]*3;
  CH(x,y,scale[0],scale[1],3);
  x+=scale[0];
  CH(x,y,scale[0],scale[1],4);
  x+=scale[0]*3;
  CH(x,y,scale[0],scale[1],3);
  x+=scale[0];
  CH(x,y,scale[0],scale[1],2);
  x+=scale[0]*4;
  
  return x;
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}


var pcolors = ['rgba(80, 80, 80, 1)', 'rgba(200, 200, 200, 1)'];
var pColorIdx = [1,1,1,0,1,1];
var pKeys = [
  [[0,0],[0,2],[4,2],[4,1],[2,1],[2,0],[0,0]],
  [[1,0],[1,1],[0,1],[0,2],[4,2],[4,1],[3,1],[3,0],[1,0]],
  [[2,0],[2,1],[0,1],[0,2],[4,2],[4,0],[2,0]],
  [[0,0],[0,1],[3,1],[3,0],[0,0]],
  [[1,0],[1,1],[0,1],[0,2],[4,2],[4,1],[3,1],[3,0],[1,0]],
  [[1.5,0],[1.5,1],[0,1],[0,2],[4,2],[4,1],[3.5,1],[3.5,0],[1,0]],
];

function CH(x,y,scaleX,scaleY,type){
  var nk = (nKey).toString(16).toUpperCase();
  scene[nk] = {
    volume : "00",
    color : "#f90",
    n: nk,
    x: x,
    y: y,
    scaleX: scaleX,
    scaleY: scaleY,
    type: type,
    update: function(tpf){

    },
    draw : function(ctx){
      
      if(this.volume != "00"){
        ctx.fillStyle = this.color;
      }
      else{
        ctx.fillStyle = pcolors[pColorIdx[this.type]];
      }

      ctx.strokeStyle="#000";
      ctx.lineWidth=1;
      
      drawKey(ctx, pKeys[this.type], this.x, this.y, this.scaleX, this.scaleY);
      
      ctx.fillStyle = "#f90";
      ctx.font = "12px Arial";
      ctx.fillText(""+this.n+"",this.x,this.y);
      ctx.stroke();
    }
  };
  nKey++;
}

function drawKey(ctx, arr, x, y, sx, sy)
{
    ctx.beginPath();
    ctx.moveTo(x+sx*arr[0][0],y+sy*arr[0][1]);
    
    for (var i = 1; i < arr.length; i++)
    {
        ctx.lineTo(x+sx*arr[i][0],y+sy*arr[i][1]);
    }
    
    ctx.fill();
    ctx.stroke();
    
    
}

