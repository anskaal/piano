var app = require('express')();
var ht = require('http');
var http = ht.Server(app);
var path = require('path');
var express = require('express');
var fs = require("fs");
var PORT = 3011;
var midiDev = "hw:2";
var client = require('socket.io-client')("http://swe.no:8080/");

app.use(express.static(path.join(__dirname, '/')));

var terminal = require('child_process');

var on = function(){
  var code = "B0 40 7F";
  console.log("on");
  terminal.spawn('amidi', ['-p',midiDev, '-S', code]);
};

app.post('/pedal/on', function(req, res){
  on();
  res.end("ok");
});

var off = function(){
  var code = "'B0 40 00'";
  console.log("off");
  terminal.spawn('amidi', ['-p',midiDev, '-S', code]);
};

app.post('/pedal/off', function(req, res){
  off();
  res.end("ok");
});

client.on('rest_call', function(data){

  if(data["action"] == "on" && data["id"] == 6){
    on();
  }
  else if(data["action"] == "off" && data["id"] == 6){
    off();
  }

  console.dir(data);
});


http.listen(PORT, function(){
  console.log('listening on *:'+PORT);
});

// Start reading from stdin so we don't exit.
process.stdin.resume();

process.on('SIGINT', function() {
  console.log('Got SIGINT.');
  midi.kill('SIGHUP');

  setTimeout(function(){
    process.exit();
  }, 1000);
});
