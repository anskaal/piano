#/bin/python
import glib, os

from pyudev import Context, Monitor

def broadcastUsbChange(action, device):
  if "BUSNUM" in device:
    print('{"action": "%s", "device": "%s"}' % (action, device["ID_MODEL_FROM_DATABASE"])) 
#  for attr in device:
#    print("attr: %s => %s" % (attr, device[attr])) 


try:
    from pyudev.glib import MonitorObserver

    def device_event(observer, device):
      broadcastUsbChange(device.action, device)

except:
    from pyudev.glib import GUDevMonitorObserver as MonitorObserver

    def device_event(observer, action, device):
      broadcastUsbChange(device.action, device)
      
context = Context()
monitor = Monitor.from_netlink(context)

monitor.filter_by(subsystem='usb')
observer = MonitorObserver(monitor)

observer.connect('device-event', device_event)
monitor.start()

glib.MainLoop().run()



